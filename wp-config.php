<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_test_local_tri' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'adminpass123' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'E_oX~|0x:`GBG^AC@Q032g.Q`WC3LS;oiQ<,TGqDMZzTbm-K!^suP  :nU{Iu:2<' );
define( 'SECURE_AUTH_KEY',  '.&o9!Y>X+8gqU2Yhx`=I1z?[Rv n[iNCmG:KUVls_XYIth$t?-7FgZ8Z53gvnzZ@' );
define( 'LOGGED_IN_KEY',    'HKD-xD9M&~A9nMwz^-`*WdP@g=3P2:FAy)ise_[_Hi(T|ls6@Jqh19]<BM&hqh,O' );
define( 'NONCE_KEY',        '+eOG0jdh%:<6>2xZWQ`:rrDr.tdd~-yOy{QeJW0%pb|%9b0?H@}&C=$Nd/{@v$ju' );
define( 'AUTH_SALT',        'W.(h5F!wcmd?mz[V;iZ]&`PHH!uw)vnVLLW8(;4[i`=d>suNkCmX~OI/GzK`PGcS' );
define( 'SECURE_AUTH_SALT', 'xnyYg #scCmY9<wLC5B)g]<LRe~v@hnn_E+dqK4Mh/UB?sc#R<oS64!muH;rc&[`' );
define( 'LOGGED_IN_SALT',   '_Z0Pw#q|(#O>[c{ZJD-RVG}N/3SgYy{2^S~CAQ/FFse0Ea{f$F62}F~[wJ[E1qFM' );
define( 'NONCE_SALT',       'R4;w)ib.O)J@97~14+L44cQbIO3JdRXWQ>V3n,8>V/(T^a_?+CJ5/$wDc)zcF$ur' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
